package at.htl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Aufgabe extends JFrame {

    private JPanel panel1;
    private JTextField txt1;
    private JTextField txt2;
    private JButton btn1;
    private JTextField txtSum;
    private JTextField txtD;
    private JTextField txtP;
    private JTextField txtQ;
    private JTextField txtM;

    public Aufgabe(String title) {

        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(panel1);
        this.pack();


        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            String a = txt1.getText();
            String b = txt2.getText();

            int x = Integer.parseInt(a);
            int y = Integer.parseInt(b);

            txtSum.setText(""+(x+y));
            txtD.setText(""+(x-y));
            txtP.setText(""+(x*y));
            double d = (double)x/(double)y;
            txtQ.setText(""+d);
            txtM.setText(""+(x%y));

            }
        });

    }
        public static void main(String[] args) {
            JFrame frame = new Aufgabe("GUI");
            frame.setVisible(true);
        }




    }
